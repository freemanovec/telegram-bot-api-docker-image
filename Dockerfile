FROM ubuntu

ENV DEBIAN_FRONTEND=noninteractive
EXPOSE 8081

WORKDIR /api
RUN apt update && apt install -y \
    git \
    build-essential \
    g++ \
    cmake \
    make \
    gperf \
    libssl-dev \
    zlib1g-dev
RUN git clone https://github.com/tdlib/telegram-bot-api .
RUN git submodule update --init --recursive
WORKDIR /api/build
RUN cmake -DCMAKE_BUILD_TYPE=Release ..
RUN cmake --build . --target install
WORKDIR /root
RUN rm -rf /api
ENTRYPOINT [ "telegram-bot-api" ]
